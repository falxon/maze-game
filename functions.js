function Line(x,y,x2,y2) {
  this.startX = x;
  this.startY = y;
  this.endX = x2;
  this.endY = y2;
  this.setup = function(){
    stroke(0);
    line(x,y,x2,y2);
  }
}
function Star(x,y) {
  this.x = x;
  this.y = y;
  this.found = false;
  this.setup = function(){
    stroke(0,230,230);
    fill(0,230,230);
    beginShape();
    vertex(x,y-8);
    vertex(x-4,y);
    vertex(x,y+8);
    vertex(x+4,y);
    endShape();
    beginShape();
    vertex(x,y-4);
    vertex(x-8,y);
    vertex(x,y+4);
    vertex(x+8,y);
    endShape();
  }
}
function movement(){
  if (charObject.up) {
    var moveUP = moveUp();
    if (moveUP) {
      charObject.y -= 1;
    }
  }
  if (charObject.down) {
    var moveDOWN = moveDown();
    if (moveDOWN) {
      charObject.y += 1;
    }
  }
  if (charObject.left) {
    var moveLEFT = moveLeft();
    if (moveLEFT) {
      charObject.x -= 1;
    }
  }
  if (charObject.right) {
    var moveRIGHT = moveRight();
    if (moveRIGHT) {
      charObject.x += 1;
    }
  }
  function moveUp(){
    var futureY = charObject.y - 1;
    for (var i = 0; i < lines.length; i++) {
      var a;
      var b;
      var c;
      var d;
      if (lines[i].startY<lines[i].endY) {
        a = lines[i].startY;
        b = lines[i].endY;
      } else {
        b = lines[i].startY;
        a = lines[i].endY;
      }
      if (lines[i].startX<lines[i].endX) {
        c = lines[i].startX;
        d = lines[i].endX;
      } else {
        d = lines[i].startX;
        c = lines[i].endX;
      }

      if (((futureY+charObject.size/2>=a)&&(futureY-charObject.size/2<=b)&&(charObject.x+charObject.size/2>=c)&&(charObject.x-charObject.size/2<=d))) {
        return false;
      }
    }
    return true;
  }
  function moveDown(){
    var futureY = charObject.y + 1;
    for (var i = 0; i < lines.length; i++) {
      var a;
      var b;
      var c;
      var d;
      if (lines[i].startY<lines[i].endY) {
        a = lines[i].startY;
        b = lines[i].endY;
      } else {
        b = lines[i].startY;
        a = lines[i].endY;
      }
      if (lines[i].startX<lines[i].endX) {
        c = lines[i].startX;
        d = lines[i].endX;
      } else {
        d = lines[i].startX;
        c = lines[i].endX;
      }

      if (((futureY+charObject.size/2>=a)&&(futureY-charObject.size/2<=b)&&(charObject.x+charObject.size/2>=c)&&(charObject.x-charObject.size/2<=d))) {
        return false;
      }
    }
    return true;
  }
  function moveLeft(){
    var futureX = charObject.x - 1;
    for (var i = 0; i < lines.length; i++) {
      var a;
      var b;
      var c;
      var d;
      if (lines[i].startY<lines[i].endY) {
        a = lines[i].startY;
        b = lines[i].endY;
      } else {
        b = lines[i].startY;
        a = lines[i].endY;
      }
      if (lines[i].startX<lines[i].endX) {
        c = lines[i].startX;
        d = lines[i].endX;
      } else {
        d = lines[i].startX;
        c = lines[i].endX;
      }

      if (((futureX+charObject.size/2>=c)&&(futureX-charObject.size/2<=d)&&(charObject.y+charObject.size/2>=a)&&(charObject.y-charObject.size/2<=b))) {
        return false;
      }
    }
    return true;
  }
  function moveRight(){
    var futureX = charObject.x + 1;
    for (var i = 0; i < lines.length; i++) {
      var a;
      var b;
      var c;
      var d;
      if (lines[i].startY<lines[i].endY) {
        a = lines[i].startY;
        b = lines[i].endY;
      } else {
        b = lines[i].startY;
        a = lines[i].endY;
      }
      if (lines[i].startX<lines[i].endX) {
        c = lines[i].startX;
        d = lines[i].endX;
      } else {
        d = lines[i].startX;
        c = lines[i].endX;
      }

      if (((futureX+charObject.size/2>=c)&&(futureX-charObject.size/2<=d)&&(charObject.y+charObject.size/2>=a)&&(charObject.y-charObject.size/2<=b))) {
        return false;
      }
    }
    return true;
  }
}
function win(){
  if ((charObject.x <= 0)||(charObject.x >= 500)||(charObject.y <= 0)||(charObject.y >= 500)) {
    charObject.escape = true;
    stroke(0);
    strokeWeight(2);
    fill(100);
    rectMode(CENTER);
    rect(width/2,height/2,300,300,3);
    fill(0);
    strokeWeight(1);
    textAlign(CENTER, CENTER);
    textSize(30);
    text("Congratulations",width/2,height/2-80);
    textSize(25);
    text("You escaped the maze",width/2,height/2-10);
    text("and won the game",width/2,height/2+20);
    text("Your score is: "+charObject.score,width/2,height/2+70);
  }
}
