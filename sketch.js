
// Declare empty arrays for maze lines and tokens
var lines = [];
var tokens = [];

// Stores data for chatacter
var charObject = {
  x: 250,
  y: 250,
  score: 0,
  escape: false,
  size: 14,
  left: false,
  right: false,
  up: false,
  down: false
};


function setup() {
  createCanvas(500,500);
  strokeWeight(1);

  // Create line objects
  for (var i = 0; i < lineCoords.length; i++) {
    var linea = new Line(lineCoords[i][0],lineCoords[i][1],lineCoords[i][2],lineCoords[i][3]);
    linea.setup();
    lines.push(linea);
  }

  // Create star objects
  for (var i = 0; i < tokenCoords.length; i++) {
    var token = new Star(tokenCoords[i][0],tokenCoords[i][1]);
    token.setup();
    tokens.push(token);
  }

}

function draw() {
  background(200);
  drawLines();
  drawTokens();
  character();
  movement();
  checkTokens();
  win();
}


// Draws character
function character(){
  fill(200,0,200);
  stroke(0);
  ellipse(charObject.x,charObject.y,charObject.size);
}

// Draws maze
function drawLines(){
  for (var i = 0; i < lines.length; i++) {
    stroke(0);
    line(lines[i].startX,lines[i].startY,lines[i].endX,lines[i].endY);
  }
}

// Draws stars
function drawTokens(){
  for (var i = 0; i < tokens.length; i++) {
    if (!tokens[i].found) {
      var x = tokens[i].x;
      var y = tokens[i].y;
      stroke(0,230,230);
      fill(0,230,230);
      beginShape();
      vertex(x,y-8);
      vertex(x-4,y);
      vertex(x,y+8);
      vertex(x+4,y);
      endShape();
      beginShape();
      vertex(x,y-4);
      vertex(x-8,y);
      vertex(x,y+4);
      vertex(x+8,y);
      endShape();
    }
  }
}

// Checks if stars have been found
function checkTokens(){
  var score = 0;
  for (var i = 0; i < tokens.length; i++) {
    if (tokens[i].found) {
      score++
    } else {
      if (dist(charObject.x,charObject.y,tokens[i].x,tokens[i].y)<=8) {
        tokens[i].found = true;
      }
    }
  }
  charObject.score = score;
}

// Detect key inputs
function keyPressed(){
  if (!charObject.escape) {
    if (keyCode === UP_ARROW) {
      charObject.up = true;
    } else {
      charObject.up = false;
    }
    if (keyCode === DOWN_ARROW) {
      charObject.down = true;
    } else {
      charObject.down = false;
    }
    if (keyCode === LEFT_ARROW) {
      charObject.left = true;
      console.log("left = true");
    } else {
      charObject.left = false;
      console.log("left = false");
    }
    if (keyCode === RIGHT_ARROW) {
      charObject.right = true;
    } else {
      charObject.right = false;
    }
  }
}
function keyReleased(){
  if (keyCode === UP_ARROW) {
    charObject.up = false;
  }
  if (keyCode === DOWN_ARROW) {
    charObject.down = false;
  }
  if (keyCode === LEFT_ARROW) {
    charObject.left = false;
    console.log("left = false");
  }
  if (keyCode === RIGHT_ARROW) {
    charObject.right = false;
  }
}
